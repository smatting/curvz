import numpy as np
import pandas as pd
import emcee
import time
import matplotlib.pyplot as plt


def load():
    fn = 'data/curves.txt'
    with open(fn, 'r') as f:
        s = f.read()
    l = filter(lambda b: len(b) > 0, s.split('\n\n'))
    xs_all, ys_all, idx_all = [], [], []
    for i, b in enumerate(l):
        xstr, ystr = b.split('\n')
        xs = [int(x) for x in xstr.split(',')]
        ys = [int(y) for y in ystr.split(',')]
        idx = [i] * len(xs)
        xs_all += xs
        ys_all += ys
        idx_all += idx
    df = pd.DataFrame({'x': xs_all, 'y': ys_all, 'curve': idx_all})
    df['x'] = df['x'] / 1000
    return df


def loglhood(theta, sigma, x, y):
    x0, c, alpha = theta
    lp = -(y - f(x, x0, c, alpha))**2 / sigma**2
    return np.sum(lp)


def f(x, x0, c, alpha):
    y = c * np.ones_like(x)
    y[x > x0] = np.power(x[x > x0] - x0, alpha) + c
    return y


def logprior(theta, x0_range, c_range, alpha_range):
    x0, c, alpha = theta
    lps = []
    for v, rng in [(x0, x0_range), (c, c_range)]:
        rng_lo, rng_hi = rng
        if (rng_lo <= v) and (v < rng_hi):
            lp = 0
        else:
            lp = -np.inf
        lps.append(lp)

    alpha_lo, alpha_hi = alpha_range
    if (alpha_lo <= alpha) and (alpha < alpha_hi):
        lp = np.log(alpha)
    else:
        lp = -np.inf
    lps.append(lp)

    return np.sum(lps)


def logpost(theta, x, y, x0_range, c_range, alpha_range, sigma):
    return logprior(theta, x0_range, c_range, alpha_range) + \
           loglhood(theta, sigma, x, y)


def sample_range(rng):
    rng_lo, rng_hi = rng
    sample = rng_lo + (rng_hi - rng_lo) * np.random.rand()
    return sample


def sample_post(x, y):
    ndim, nwalkers = 3, 100

    x0_range = [0, 20]
    c_range = [-20, 20]
    alpha_range = [1, 7]
    sigma = 20

    sampler = emcee.EnsembleSampler(nwalkers, ndim, logpost,
                                    args=(x, y, x0_range, c_range,
                                          alpha_range, sigma))

    np.random.seed(int(time.time() * 100) % 4294967295)
    theta_start = [[sample_range(x0_range),
                    sample_range(c_range),
                    sample_range(alpha_range)] for i in range(nwalkers)]

    nsamples = 1000
    nburnin = 300
    sampler.run_mcmc(theta_start, nsamples)
    samples = sampler.chain[:, nburnin:, :].reshape((-1, ndim))
    df = pd.DataFrame(samples)
    return df


def main():
    for i in range(10):
        df = load()
        print(i)
        df = df[df.curve == i+1]
        x = df['x'].values
        y = df['y'].values
        sample_df = sample_post(x, y)
        x0, c, alpha = sample_df.mean()
        xs = np.linspace(x.min(), x.max(), 100)
        ys = f(xs, x0, c, alpha)

        fig, ax = plt.subplots()
        ax.scatter(x, y)
        plt.savefig('plots/points-{}.png'.format(i))
        ax.plot(xs, ys, 'r')
        plt.savefig('plots/curve-{}.png'.format(i))
        plt.close()



