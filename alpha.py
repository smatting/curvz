import numpy as np
import emcee
import matplotlib.pyplot as plt


def sample_uniform(nsamples):
    alphas = np.random.uniform(1, 4, nsamples)
    return alphas


def plike(alpha):
    if (alpha >= 1.0) and (alpha < 4.0):
        return alpha
    else:
        return -np.inf


def sample_id(nsamples):
    nburnin = nsamples // 3
    nwalkers = 20
    ndim = 1
    sampler = emcee.EnsembleSampler(nwalkers, ndim, plike)
    start = [[np.random.uniform(1, 4)] for i in range(nwalkers)]
    n = (nburnin + nsamples // nwalkers)
    sampler.run_mcmc(start, n)
    samples = sampler.chain[:, nburnin:, :].reshape((-1, ))
    return samples


def plot_alphas(alphas, fn='alphas.png'):
    x = np.linspace(0., 5., 100)
    plt.close()
    fig, ax = plt.subplots()
    for alpha in alphas:
        ax.plot(x, np.power(x, alpha), 'b', alpha=0.01, lw=4)
    plt.savefig(fn)


def main():
    nsamples = 400
    plot_alphas(sample_uniform(nsamples), 'plots/alphas_uniform.png')
    plot_alphas(sample_id(nsamples), 'plots/alphas_id.png')
